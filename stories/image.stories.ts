import { Meta, Story } from '@storybook/angular/types-6-0'
import { ImageComponent } from 'projects/kalyptio-angular-components/src/lib/components/image/image.component';

export default {
  title: 'Components/Image',
  component: ImageComponent
} as Meta;

const Template: Story<ImageComponent> = (args) => ({
  props: args,
})

export const Primary = Template.bind({});

Primary.args = {
  src: 'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg',
  thumbnail: 'https://shopihunter.com/wp-content/uploads/2020/05/31c53f9816acc0a1ca3df6d959b9078e-3.jpg',
  figcaptionRef:'https://www.freepik.com/photos/sale',
  figcaptionText: 'Sale photo created by freepik - www.freepik.com'
}