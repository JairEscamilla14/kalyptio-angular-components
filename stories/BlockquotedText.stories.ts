import { Meta, Story } from '@storybook/angular/types-6-0';
import { BlockquotedTextComponent } from './../projects/kalyptio-angular-components/src/lib/components/blockquoted-text/blockquoted-text.component';

export default {
  title: 'Components/BlockquotedText',
  component: BlockquotedTextComponent
} as Meta;

const Template: Story<BlockquotedTextComponent> = (args) => ({
  props: args
});

export const Primary = Template.bind({});

Primary.args = {
  text: 'Blockquoted Text Works!'
}