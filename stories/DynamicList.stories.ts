import { Meta, Story } from '@storybook/angular/types-6-0';
import { DynamicListComponent } from './../projects/kalyptio-angular-components/src/lib/components/dynamic-list/dynamic-list/dynamic-list.component';
import { moduleMetadata } from '@storybook/angular';
import { DynamicListItemComponent } from 'projects/kalyptio-angular-components/src/lib/components/dynamic-list/dynamic-list-item/dynamic-list-item.component';
import { CommonModule } from '@angular/common';
import { NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { ImageComponent } from 'projects/kalyptio-angular-components/src/lib/components/image/image.component';
import {
  beneficiosFirstList,
  beneficiosSecondList,
} from 'projects/kalyptio-angular-components/src/lib/components/dynamic-list/dynamic-list/dynamic-list';
import { DynamicListTitleComponent } from 'projects/kalyptio-angular-components/src/lib/components/dynamic-list/dynamic-list-title/dynamic-list-title.component';

export default {
  title: 'Components/DynamicList',
  component: DynamicListComponent,
  decorators: [
    moduleMetadata({
      declarations: [
        DynamicListTitleComponent,
        DynamicListItemComponent,
        ImageComponent,
      ],
      imports: [CommonModule, NgbNavModule],
    }),
  ],
} as Meta;

const Template: Story<DynamicListComponent> = (args) => ({
  props: args,
  template: `
    <lib-dynamic-list [variant]="variant" [listItems]="listItems">

    </lib-dynamic-list>
  `,
});

export const Primary = Template.bind({});
export const Secondary = Template.bind({});

Primary.args = {
  variant: 'primary',
  listItems: beneficiosFirstList,
};

Secondary.args = {
  variant: 'secondary',
  listItems: beneficiosSecondList,
};
