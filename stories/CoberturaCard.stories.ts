import { Meta, Story } from '@storybook/angular/types-6-0'
import { CoberturaCardComponent } from './../projects/kalyptio-angular-components/src/lib/components/cobertura-card/cobertura-card.component';
import { moduleMetadata } from '@storybook/angular';
import { CardComponent } from './../projects/kalyptio-angular-components/src/lib/components/card/card.component';
import { CommonModule } from '@angular/common';
import { ImageComponent } from 'projects/kalyptio-angular-components/src/lib/components/image/image.component';

export default {
  title: 'Components/CoberturaCard',
  component: CoberturaCardComponent,
  decorators: [
    moduleMetadata({
      declarations: [CardComponent, ImageComponent],
      imports: [CommonModule]
    })
  ]
} as Meta;

const Template: Story<CoberturaCardComponent> = (args) => ({
  props: args,
});

export const Primary = Template.bind({});

Primary.args = {
  cardTitle: 'Gastos médicos',
  cardDescription: 'Cobertura de gastos médicos hasta por 35,000 MXN.',
  image: 'https://animaliapetcare.mx/assets/images/heart1.svg',
  imageWidth: '30%'
}