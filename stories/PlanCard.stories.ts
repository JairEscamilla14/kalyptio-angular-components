import { Meta, Story } from '@storybook/angular/types-6-0';
import {
  PlanCardComponent,
  PlanCardType,
} from '../projects/kalyptio-angular-components/src/lib/components/plan-card/plan-card.component';
import { moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { ImageComponent } from 'projects/kalyptio-angular-components/src/lib/components/image/image.component';
ButtonComponent;
import { ButtonComponent } from '../projects/kalyptio-angular-components/src/lib/components/button/button.component';

type PlanCardStory = {
  plan: PlanCardType;
  contrataUrl: string;
};

const PlanCardProps = {
  plan: {
    title: 'Platino',
    price: '4,163.24',
    bulletPoints: [
      {
        title: 'Gastos médicos',
        price: '35,000.00',
      },
      {
        title: 'Responsabilidad civil',
        price: '60,000.00',
      },
      {
        title: 'Costo servicio funerario',
        price: '1,500.00',
      },
    ],
    morePopular: false,
  },
  variant: 'primary',
  contrataUrl: 'https://seguros.animaliapetcare.mx/?plan=2',
};

export default {
  title: 'Components/PlanCard',
  component: PlanCardComponent,
  decorators: [
    moduleMetadata({
      declarations: [PlanCardComponent, ImageComponent, ButtonComponent],
      imports: [CommonModule],
    }),
  ],
} as Meta;

const Template: Story<PlanCardStory> = (args) => ({
  props: args,
  template: `
    <lib-plan-card [plan]="plan" [variant]="variant" [contrataUrl]="contrataUrl" [backgroundImage]="backgroundImage || ''">
      <img class="money-sign" src="assets/images/money-sign-0.png" alt="" money-sign-image />
      <li class="text-center">Los elementos de la lista y la imagen de dolar serán definidos</li>
      <li class="text-center">por el componente que este implementando</li>
      <li class="text-center">la tarjeta</li>
    </lib-plan-card>
  `,
});

export const Primary = Template.bind({});
export const Secondary = Template.bind({});
export const MorePopular = Template.bind({});

Primary.args = {
  ...PlanCardProps,
};

Secondary.args = {
  plan: {
    ...PlanCardProps.plan,
  },
  variant: 'secondary',
  contrataUrl: PlanCardProps.contrataUrl,
};

MorePopular.args = {
  plan: {
    ...PlanCardProps.plan,
    morePopular: true,
  },
  backgroundImage: '/images/Oval.png',
  variant: 'primary',
  contrataUrl: PlanCardProps.contrataUrl,
};
