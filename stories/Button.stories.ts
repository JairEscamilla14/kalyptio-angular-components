import { Meta, Story } from "@storybook/angular/types-6-0";
import { ButtonComponent } from './../projects/kalyptio-angular-components/src/lib/components/button/button.component';

export default {
  title: 'Components/Button',
  component: ButtonComponent
} as Meta;

const Template: Story<ButtonComponent> = (args) => ({
  props: args,
  template: `
    <lib-button [buttonText]="buttonText" [icon]="icon" [variant]="variant">
    </lib-button>
  `
});

const buttonProps = {
  buttonText: 'Mira cómo funciona',
  icon: 'play_circle_outline'
}

export const Orange = Template.bind({});
export const LightBlue = Template.bind({});
export const DarkenBlue = Template.bind({});
export const WithoutIcon = Template.bind({});



Orange.args = {
  ...buttonProps,
  variant: 'orange'
}

LightBlue.args = {
  ...buttonProps,
  variant: 'light-blue'
}

DarkenBlue.args = {
  ...buttonProps,
  variant: 'darken-blue'
}

WithoutIcon.args = {
  ...buttonProps,
  icon: undefined
}