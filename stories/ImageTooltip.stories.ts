import { Meta, Story } from '@storybook/angular/types-6-0';
import { ImageTooltipComponent } from './../projects/kalyptio-angular-components/src/lib/components/image-tooltip/image-tooltip.component';
import { moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { NgbModule, NgbTooltipConfig } from '@ng-bootstrap/ng-bootstrap';

export default {
  title: 'Components/ImageTooltip',
  component: ImageTooltipComponent,
  decorators: [
    moduleMetadata({
      imports: [
        CommonModule,
        NgbModule,
      ]
    })
  ]
} as Meta;

const Template: Story<ImageTooltipComponent> = (args) => ({
  props: args,
});


export const Primary = Template.bind({});

Primary.args = {
  image: 'https://upload.wikimedia.org/wikipedia/commons/a/a4/Mastercard_2019_logo.svg',
  tooltip: 'Mastercard',
  alt: 'Master Card image'
}