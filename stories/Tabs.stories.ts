import { Meta, Story } from '@storybook/angular/types-6-0';
import { TabsComponent } from './../projects/kalyptio-angular-components/src/lib/components/tabs/tabs/tabs.component';
import { moduleMetadata } from '@storybook/angular';
import { TabTitleComponent } from './../projects/kalyptio-angular-components/src/lib/components/tabs/tab-title/tab-title.component';
import { CommonModule } from '@angular/common';
import { TabBodyComponent } from 'projects/kalyptio-angular-components/src/lib/components/tabs/tab-body/tab-body.component';

export default {
  title: 'Components/tabs',
  component: TabsComponent,
  decorators: [
    moduleMetadata({
      declarations: [
        TabTitleComponent,
        TabBodyComponent
      ],
      imports: [
        CommonModule
      ]
    })
  ]
} as Meta;

const Template: Story<TabsComponent> = (args) => ({
  props: args,
  template: `
    <lib-tabs [tabsTitles]="tabsTitles" [withIndex]="withIndex">
      <lib-tab-body>
        Contenido de la primer tab
      </lib-tab-body>
      <lib-tab-body>
        Contenido de la segunda tab
      </lib-tab-body>
      <lib-tab-body>
        Contenido de la tercer tab
      </lib-tab-body>
    </lib-tabs>
  `
})

export const Primary = Template.bind({});

Primary.args = {
  tabsTitles: [
    {
      title: '¿Contra qué me cubre el seguro?',
      description: 'Nuestro seguro es muy completo, infórmate para conocer la cobertura y estar protegido.',
      isActive: true
    },
    {
      title: 'Protección para daños a terceros',
      description: 'A veces los accidentes llegan a perjudicar a un tercero y nos pueden meter en un lío que incurran más gastos.',
      isActive: false
    },
    {
      title: 'Protección para daños a terceros',
      description: 'A veces los accidentes llegan a perjudicar a un tercero y nos pueden meter en un lío que incurran más gastos.',
      isActive: false
    },
  ],
  withIndex: true
}