import { Meta, Story } from '@storybook/angular/types-6-0'
import { CardComponent } from './../projects/kalyptio-angular-components/src/lib/components/card/card.component';

export default {
  title: 'Components/Card',
  component: CardComponent
} as Meta;

const Template: Story<CardComponent> = (args) => ({
  props: args,
  template: `
    <lib-card>
      <div style="height: 200px" class="mt-5">
        Componente genérico de Card
      </div>
    </lib-card>
  `
})

export const Primary = Template.bind({});