import { Meta, Story } from '@storybook/angular/types-6-0';
import { ClientReviewCardComponent } from 'projects/kalyptio-angular-components/src/lib/components/client-review-card/client-review-card.component';

export default {
  title: 'Components/ClientReviewCard',
  component: ClientReviewCardComponent
} as Meta;

const Template: Story<ClientReviewCardComponent> = (args) => ({
  props: args,
})

export const Primary = Template.bind({});

Primary.args = {
  review: {
    stars: 5,
    avatar: "https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlciUyMHByb2ZpbGV8ZW58MHx8MHx8&ixlib=rb-1.2.1&w=1000&q=80",
    review: `"It seems that only fragments of the original text remain in the Lorem Ipsum texts used today."`,
    client: "Thomas Israel",
    clientPosition: "C.E.O"
  }
}