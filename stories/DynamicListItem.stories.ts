import { Meta, Story } from '@storybook/angular/types-6-0';
import { DynamicListItemComponent } from '../projects/kalyptio-angular-components/src/lib/components/dynamic-list/dynamic-list-item/dynamic-list-item.component';
import { beneficiosFirstList } from './../projects/kalyptio-angular-components/src/lib/components/dynamic-list/dynamic-list/dynamic-list';

export default {
  title: 'Components/DynamicListItem',
  component: DynamicListItemComponent
} as Meta;

const Template: Story<DynamicListItemComponent> = (args) => ({
  props: args,
  template: `
    <lib-dynamic-list-item [item]="item">
    </lib-dynamic-list-item>
  `
});

export const Primary = Template.bind({});

Primary.args = {
  item: beneficiosFirstList[0]
}