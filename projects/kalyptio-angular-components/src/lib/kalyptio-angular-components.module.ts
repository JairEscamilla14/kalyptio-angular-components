import { NgModule } from '@angular/core';
import { PruebaComponent } from './components/prueba/prueba.component';
import { PlanCardComponent } from './components/plan-card/plan-card.component';
import { ImageComponent } from './components/image/image.component';
import { TabTitleComponent } from './components/tabs/tab-title/tab-title.component';
import { CardComponent } from './components/card/card.component';
import { CoberturaCardComponent } from './components/cobertura-card/cobertura-card.component';
import { NgbNavModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DynamicListComponent } from './components/dynamic-list/dynamic-list/dynamic-list.component';
import { TabBodyComponent } from './components/tabs/tab-body/tab-body.component';
import { TabsComponent } from './components/tabs/tabs/tabs.component';
import { ClientReviewCardComponent } from './components/client-review-card/client-review-card.component';
import { ImageTooltipComponent } from './components/image-tooltip/image-tooltip.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DynamicListItemComponent } from './components/dynamic-list/dynamic-list-item/dynamic-list-item.component';

import { BlockquotedTextComponent } from './components/blockquoted-text/blockquoted-text.component';
import { ButtonComponent } from './components/button/button.component';
import { DynamicListTitleComponent } from './components/dynamic-list/dynamic-list-title/dynamic-list-title.component';

@NgModule({
  declarations: [
    PruebaComponent,
    PlanCardComponent,
    ImageComponent,
    CardComponent,
    CoberturaCardComponent,
    DynamicListComponent,
    TabBodyComponent,
    TabsComponent,
    TabTitleComponent,
    ClientReviewCardComponent,
    ImageTooltipComponent,
    DynamicListItemComponent,
    BlockquotedTextComponent,
    DynamicListTitleComponent,
    ButtonComponent,
  ],
  imports: [NgbNavModule, NgbModule, RouterModule, CommonModule],
  exports: [
    PruebaComponent,
    PlanCardComponent,
    ImageComponent,
    CardComponent,
    DynamicListComponent,
    ClientReviewCardComponent,
    ImageTooltipComponent,
    TabsComponent,
    TabBodyComponent,
    TabTitleComponent,
    DynamicListItemComponent,
    CoberturaCardComponent,
    BlockquotedTextComponent,
    ButtonComponent,
    DynamicListTitleComponent,
  ],
})
export class KalyptioAngularComponentsModule {}
