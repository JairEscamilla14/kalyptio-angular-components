/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { BlockquotedTextComponent } from './blockquoted-text.component';

describe('BlockquotedTextComponent', () => {
  let component: BlockquotedTextComponent;
  let fixture: ComponentFixture<BlockquotedTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockquotedTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockquotedTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
