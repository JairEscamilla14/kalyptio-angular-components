import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'lib-blockquoted-text',
  templateUrl: './blockquoted-text.component.html',
  styleUrls: ['./blockquoted-text.component.css']
})
export class BlockquotedTextComponent implements OnInit {

  @Input() text: string = "";

  constructor() { }

  ngOnInit() {
  }

}
