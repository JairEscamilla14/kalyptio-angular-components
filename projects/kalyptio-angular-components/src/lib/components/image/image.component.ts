import { Component, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';

@Component({
  selector: 'lib-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements AfterViewInit {
  @Input() thumbnail = "";
  @Input() src = "";
  @Input() width = "100%";
  @Input() height = "auto";
  @Input() figcaptionRef = "";
  @Input() figcaptionText = "";
  @ViewChild('image') image: ElementRef;

  newImageIsSetted: boolean = false;

  private observer: IntersectionObserver | undefined;
  private intersectionObserverOptions = {
    rootMargin: '0px',
    threshold: 0.2
  }

  constructor() { }

  ngAfterViewInit() {
    this.observer = new IntersectionObserver(
      this.intersectionObserverCallback.bind(this),
      this.intersectionObserverOptions
    );
    this.observer.observe(this.image.nativeElement);
  }

  intersectionObserverCallback(entries) {
    const { isIntersecting } = entries[0];
    if (isIntersecting) {
      this.newImageIsSetted = true;
      this.image.nativeElement.src = this.src;
      this.observer.disconnect();
    }
  }
}
