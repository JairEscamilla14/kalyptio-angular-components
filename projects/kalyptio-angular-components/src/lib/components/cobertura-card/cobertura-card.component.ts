import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'lib-cobertura-card',
  templateUrl: './cobertura-card.component.html',
  styleUrls: ['./cobertura-card.component.css']
})
export class CoberturaCardComponent implements OnInit {
  @Input() image: string = "";
  @Input() imageThumbnail: string = ""
  @Input() imageWidth: string = "";
  @Input() cardTitle: string = "";
  @Input() cardDescription: string = "";
  constructor() { }

  ngOnInit(): void {
  }

}
