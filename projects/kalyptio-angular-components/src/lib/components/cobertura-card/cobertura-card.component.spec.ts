import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoberturaCardComponent } from './cobertura-card.component';

describe('CoberturaCardComponent', () => {
  let component: CoberturaCardComponent;
  let fixture: ComponentFixture<CoberturaCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoberturaCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoberturaCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
