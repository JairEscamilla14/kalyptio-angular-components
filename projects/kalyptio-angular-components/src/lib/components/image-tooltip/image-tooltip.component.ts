import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'lib-image-tooltip',
  templateUrl: './image-tooltip.component.html',
  styleUrls: ['./image-tooltip.component.css']
})
export class ImageTooltipComponent implements OnInit {
  @Input() image: string = "";
  @Input() tooltip: string = "";
  @Input() alt: string = "";

  constructor() { }

  ngOnInit() {
  }

}
