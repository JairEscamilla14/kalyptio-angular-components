import { Component, OnInit, Input, ViewChildren, QueryList, AfterViewInit, ElementRef } from '@angular/core';
import { TabTitleType } from '../tab-title/tab-title.component';

@Component({
  selector: 'lib-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.css']
})
export class TabsComponent implements OnInit, AfterViewInit {

  @Input() tabsTitles: TabTitleType[] = [];
  @Input() withIndex = true;
  @ViewChildren('tabs') tabs: QueryList<ElementRef>;

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.tabs.first.nativeElement.children[0].firstChild.classList.add("active-tab")
  }
  
  changeActiveTab(newActiveIndex: number) {
    const tabsElement = this.tabs.first.nativeElement;
    const numberOfChildren = tabsElement.children.length;
    for(let i = 0; i < numberOfChildren; i++) {
      const element = tabsElement.children[i].firstChild;
      if(newActiveIndex === i){
        element.classList.add("active-tab");
      }else{
        element.classList.remove("active-tab");
      }
    }
    this.tabsTitles = this.tabsTitles.map((tabTitle, index) => (
      {
        ...tabTitle,
        isActive: newActiveIndex === index
      }
    ))
  }
}
