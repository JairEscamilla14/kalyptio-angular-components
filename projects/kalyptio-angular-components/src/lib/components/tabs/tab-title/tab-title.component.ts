import { Component, Input, OnInit } from '@angular/core';

export type TabTitleType = {
  title: string;
  description: string;
  isActive: boolean;
}

@Component({
  selector: 'lib-tab-title',
  templateUrl: './tab-title.component.html',
  styleUrls: ['./tab-title.component.css']
})
export class TabTitleComponent implements OnInit {
  @Input() tabTitle: TabTitleType;
  @Input() index = 0;
  @Input() withIndex = true;
  constructor() { }

  ngOnInit(): void {
  }

}
