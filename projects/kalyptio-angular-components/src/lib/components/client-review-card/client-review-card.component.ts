import { Component, Input, OnInit } from '@angular/core';

export type clientReview = {
  stars: number;
  avatar: string;
  review: string;
  client: string;
  clientPosition: string;
}

@Component({
  selector: 'lib-client-review-card',
  templateUrl: './client-review-card.component.html',
  styleUrls: ['./client-review-card.component.css']
})
export class ClientReviewCardComponent implements OnInit {
  @Input() review: clientReview;
  constructor() { }

  ngOnInit(): void {
  }

}
