import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientReviewCardComponent } from './client-review-card.component';

describe('ClientReviewsCarouselSlideComponent', () => {
  let component: ClientReviewCardComponent;
  let fixture: ComponentFixture<ClientReviewCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClientReviewCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientReviewCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
