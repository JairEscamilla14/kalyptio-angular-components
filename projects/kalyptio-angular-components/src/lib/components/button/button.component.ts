import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


export type ButtonVariants = 'orange' | 'light-blue' | 'darken-blue';
export type ButtonType = 'button' | 'submit' | 'reset'

@Component({
  selector: 'lib-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {

  @Input() variant: ButtonVariants = 'orange';
  @Input() buttonText: string = "";
  @Input() icon: string | undefined = undefined;
  @Input() type: ButtonType = 'button';
  @Output() buttonClicked = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  buttonClick() {
    this.buttonClicked.emit(this.buttonText);
  }

}
