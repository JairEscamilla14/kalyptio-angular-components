import { Component, Input, OnInit } from '@angular/core';
import { DynamicListItemType } from '../dynamic-list-item/dynamic-list-item.component';
@Component({
  selector: 'lib-dynamic-list',
  templateUrl: './dynamic-list.component.html',
  styleUrls: ['./dynamic-list.component.css']
})
export class DynamicListComponent implements OnInit {

  @Input() listItems: DynamicListItemType[] = [];
  @Input() variant: 'primary' | 'secondary' = 'primary';

  constructor() { }

  ngOnInit(): void {
  }
}
