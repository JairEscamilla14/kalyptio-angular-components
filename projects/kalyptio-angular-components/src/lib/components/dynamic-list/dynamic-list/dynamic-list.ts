import { DynamicListItemType } from '../dynamic-list-item/dynamic-list-item.component';

export const beneficiosFirstList: DynamicListItemType[] = [
  {
    title: 'Servicio de estética',
    shortDescription:
      'Tu mascota merece que la mimes y con nuestro servicio completo de estética canina y felina quedará reluciente',
    image: `https://www.eluniversal.com.mx/sites/default/files/2020/04/02/pets.jpg`,
    thumbnail: `https://www.eluniversal.com.mx/sites/default/files/2020/04/02/pets.jpg`,
    description:
      'Una vez por vigencia de póliza podrás regalarle a tu mascota  un servicio de estética completa que incluye: baño, corte de cabello, corte de uñas y cepillado. Consulta las tarifas de este beneficio descargando este PDF.',
    figcaptionRef: 'https://www.freepik.com/photos/sale',
    figcaptionText: 'Sale photo created by freepik - www.freepik.com',
    link: 'https://animaliapetcare.mx/',
    linkText: 'Descargando este PDF',
    isInCarousel: true,
  },
  {
    title: 'Servicio de baño',
    shortDescription:
      'Un baño al año no hace daño. Obtén acceso a un baño en salones especializados.',
    image: `https://www.eluniversal.com.mx/sites/default/files/2020/04/02/pets.jpg`,
    thumbnail: `https://www.eluniversal.com.mx/sites/default/files/2020/04/02/pets.jpg`,
    description:
      'Una vez por vigencia de póliza podrás regalarle a tu mascota un baño en un salón especializado en peludos. En caso de reembolso tenemos tarifas especiales. Consulta las tarifas de este beneficio descargando este PDF.',
  },
  {
    title: 'Salud Preventiva',
    shortDescription:
      'Prevenir es cuidar. Te ofrecemos valoraciones por medio de video consultas con veterinarios así como vacunas.',
    image: `https://www.eluniversal.com.mx/sites/default/files/2020/04/02/pets.jpg`,
    thumbnail: `https://www.eluniversal.com.mx/sites/default/files/2020/04/02/pets.jpg`,
    description:
      'Por vigencia de póliza tendras acceso a dos video consultas de valoración gratuitas así como una vacuna antirrábica o una desparacitación (estas aplican por reembolso hasta $200.00 M.N.).',
  },
  {
    title: 'Servicios y Descuentos',
    shortDescription:
      'Obtén acceso exclusivo a descuentos en diversos servicios y productos gracias a las alianzas que se tienen.',
    image: `https://www.eluniversal.com.mx/sites/default/files/2020/04/02/pets.jpg`,
    thumbnail: `https://www.eluniversal.com.mx/sites/default/files/2020/04/02/pets.jpg`,
    description:
      'Podrás tener descuentos en: accesorios de adiestramiento, alimentos secos, húmedos y holísticos, hospitales clínicos, estéticas, revistas y eventos, spas, fotografías, guarderías y paseadores, etc. Consulta la lista completa de este beneficio descargando este PDF.',
  },
];

export const beneficiosSecondList: DynamicListItemType[] = [
  {
    title: 'Asesoría Legal Telefónica Ilimitada',
    shortDescription:
      'A veces nuestras mascotas hacen cosas que nos pueden perjudicar, pero no estás solo, nosotros te protegemos.',
    image: `https://www.eluniversal.com.mx/sites/default/files/2020/04/02/pets.jpg`,
    thumbnail: `https://www.eluniversal.com.mx/sites/default/files/2020/04/02/pets.jpg`,
    description:
      'Obtén acceso ilimitado a asesoría legal telefónica la cual podrá ser usada: en caso de robo o extravío de mascota, concerniente a sanciones, en quejas de maltrato animal, para conocer las obligaciones y derechos como dueño de mascota y para pasos y acciones cuando hay daños a terceros.',
  },
  {
    title: 'Asesoría Veterinaria Telefónica Ilimitada',
    shortDescription:
      'Grandes médicos veterinarios a tan solo una llamada de distancia.',
    image: `https://www.eluniversal.com.mx/sites/default/files/2020/04/02/pets.jpg`,
    thumbnail: `https://www.eluniversal.com.mx/sites/default/files/2020/04/02/pets.jpg`,
    description:
      'Todo el mundo ha tenido dudas médicas sobre sus mascotas en algún punto. Asesórate con nuestra red de médicos veterinarios y soluciona diferentes casos como: emergencias, nutrición, orientación de vacunación y más. Consulta la lista completa de este beneficio descargando este PDF.',
  },
  {
    title: 'Concierge ilimitado',
    shortDescription:
      'Ponemos a tu disposición la ayuda que necesitas por medio del beneficio de tener concierge ilimitado.',
    image: `https://www.eluniversal.com.mx/sites/default/files/2020/04/02/pets.jpg`,
    thumbnail: `https://www.eluniversal.com.mx/sites/default/files/2020/04/02/pets.jpg`,
    description:
      'Obtén orientación y apoyo en el manejo de chip (en caso de tener), información respecto a campañas de vacunación o esterilización, exposiciones y ferias, restaurantes y hoteles pet friendly y más. Consulta la lista completa de este beneficio descargando este PDF.',
  },
  {
    title: 'Hospedaje para mascota',
    shortDescription:
      '¿Qué le pasa a tu mascota si a ti te hospitalizan? No siempre tendrás oportunidad de planear quién se quede con ella.',
    image: `https://www.eluniversal.com.mx/sites/default/files/2020/04/02/pets.jpg`,
    thumbnail: `https://www.eluniversal.com.mx/sites/default/files/2020/04/02/pets.jpg`,
    description:
      'En caso de que el dueño de la mascota sea hospitalizado por enfermedad o accidente, la mascota contará hasta con 4 noches de hospedaje sin ningún costo extra. (Un evento por vigencia de la póliza)',
  },
];
