import { Component, Input, OnInit } from '@angular/core';

export type DynamicListItemType = {
  title: string;
  shortDescription: string;
  image: string;
  thumbnail: string;
  figcaptionRef?: string;
  figcaptionText?: string;
  description: string;
  link?: string;
  linkText?: string;
  isInCarousel?: boolean;
};

export type itemVariant = 'normal' | 'carousel';

@Component({
  selector: 'lib-dynamic-list-item',
  templateUrl: './dynamic-list-item.component.html',
  styleUrls: ['./dynamic-list-item.component.css'],
})
export class DynamicListItemComponent implements OnInit {
  @Input() item: DynamicListItemType;
  @Input() variant: itemVariant = 'normal';
  constructor() {}

  ngOnInit(): void {}
}
