import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'lib-dynamic-list-title',
  templateUrl: './dynamic-list-title.component.html',
  styleUrls: ['./dynamic-list-title.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DynamicListTitleComponent implements OnInit {
  @Input() title: string;
  @Input() description: string;
  constructor() { }

  ngOnInit(): void {
  }

}
