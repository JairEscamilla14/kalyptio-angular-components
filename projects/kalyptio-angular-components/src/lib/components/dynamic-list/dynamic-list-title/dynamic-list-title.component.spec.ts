import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicListTitleComponent } from './dynamic-list-title.component';

describe('DynamicListTitleComponent', () => {
  let component: DynamicListTitleComponent;
  let fixture: ComponentFixture<DynamicListTitleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DynamicListTitleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicListTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
