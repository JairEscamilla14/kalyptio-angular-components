import { Component, Input, OnInit } from '@angular/core';

export interface bulletPoint {
  title: string;
  price: string;
}
export interface PlanCardType {
  title: string;
  price: string;
  bulletPoints: bulletPoint[];
  morePopular?: boolean;
}

@Component({
  selector: 'lib-plan-card',
  templateUrl: './plan-card.component.html',
  styleUrls: ['./plan-card.component.css'],
})
export class PlanCardComponent implements OnInit {
  @Input() plan: PlanCardType;
  @Input() backgroundImage = '';
  @Input() contrataUrl: string;
  @Input() variant: 'primary' | 'secondary' = 'primary';

  backgroundUrl = '';

  constructor() {}

  ngOnInit(): void {
    this.backgroundUrl = `url(${this.backgroundImage})`;
  }

  contratar() {
    window.open(this.contrataUrl, '_blank');
  }
}
