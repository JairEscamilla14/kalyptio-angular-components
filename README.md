# Kalyptio Angular Components
## Esta es una libería de componentes genéricos para ser usados en los proyectos de Kalyptio que esten hechos en Angular.

### Correr storybook en local:

```sh
npm run storybook
```

### Hacer deploy de storybook hacia chromatic:
```sh
npm run chromatic
```

### Subir actualización de la librería hacia npm: 
```sh
ng build --prod
cd dist/kalyptio-angular-components
npm publish --ignore-scripts
```
